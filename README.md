# Node-JSON-Redis-exp1
experiment with Node, JSON and Redis 

Description
    A simple expression application for storing JSON data to Redis Database.
    It uses ReJSON module from Redis.

Installation Guide

    - Local Environment
        1. Install node.
        2. npm install
        3. node app.js

    - Docker
        1. docker build -t json2redis .
        2. docker-compose up

